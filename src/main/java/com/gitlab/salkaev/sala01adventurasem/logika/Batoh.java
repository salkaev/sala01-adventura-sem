/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package com.gitlab.salkaev.sala01adventurasem.logika;

import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;


/*******************************************************************************
 * Instance třídy {@code Batoh} představují ...
 * The {@code Batoh} class instances represent ...
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class Batoh implements Subject
{
    private Map<String,Vec> veciVBatohu;
    private final int MAX_VELIKOST = 4;
    private Set<Observer> observerSet;
    /***************************************************************************
     */
    public Batoh()
    {
        veciVBatohu = new HashMap<>();
        observerSet = new HashSet<>();
    }
    
    public String pridatVec(Vec vec){
        if(MAX_VELIKOST == veciVBatohu.size()){
            return "V batohu neni mista";
        }else{
            veciVBatohu.put(vec.getNazevVeci(),vec);
            notifyObservers();
            return "Uložil jsi do batohu " + vec.getNazevVeci();
        }
    }
    
    public boolean jeVBatohu(String vec){
        return veciVBatohu.containsKey(vec);
    }
    
    public Vec odebratVec(String vec){
        return veciVBatohu.remove(vec);
    }
    
    public String getSeznam(){
        String seznam ="";
        for(Vec vec: veciVBatohu.values()){
            seznam += seznam == "" ? " " + vec.getNazevVeci(): ", " + vec.getNazevVeci();
        }
        return "V batohu je:" + (seznam.equals("")? "žadna vec v batohu" :" " + seznam);
    }

    public Map<String,Vec> getVeciVBatohu(){
        return veciVBatohu;

    }

    @Override
    public void register(Observer obj) {
        observerSet.add(obj);
    }

    @Override
    public void unregister(Observer obj) {
        observerSet.remove(obj);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer: observerSet) {
            observer.update();
        }
    }
}
