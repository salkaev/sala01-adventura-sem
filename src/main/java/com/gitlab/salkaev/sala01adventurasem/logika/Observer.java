package com.gitlab.salkaev.sala01adventurasem.logika;

public interface Observer {

    public void update();

}
