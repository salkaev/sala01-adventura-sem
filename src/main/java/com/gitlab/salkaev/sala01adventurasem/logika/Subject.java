package com.gitlab.salkaev.sala01adventurasem.logika;

public interface Subject {

    public void register(Observer obj);

    public void unregister(Observer obj);

    public void notifyObservers();

}
