package com.gitlab.salkaev.sala01adventurasem.main;

import com.gitlab.salkaev.sala01adventurasem.logika.*;
import com.gitlab.salkaev.sala01adventurasem.logika.Observer;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.image.ImageView;
import java.io.IOException;
import java.util.*;


public class Controller extends VBox implements Observer
{
    IHra hra;
    Stage primaryStage;

    @FXML public TextField terminalText;
    @FXML public TextArea textShow;
    @FXML public Text penizeText;
    @FXML public MenuBar myMenuBar;
    @FXML public ListView seznamVychodu;
    @FXML public Button submitButton;
    @FXML public Button submitOtaznik;
    @FXML public ImageView hrac;
    @FXML public ImageView klobasaInv;
    @FXML public ImageView chlebaInv;
    @FXML public ImageView klicInv;
    @FXML public ImageView penizeInv;
    @FXML public ImageView chlebaKlobasaInv;


    private Map<String,Point2D> souradnice;


    /**
     * funkce pro vytvoreni souradnice ktere slouzi pro spravne fungovani mapy
     */
    private void vytvorSouradnice() {
        souradnice = new HashMap<>();
        souradnice.put("vlastní_pokoj", new Point2D(102, 60));
        souradnice.put("obývací_pokoj", new Point2D(94, 119));
        souradnice.put("kuchyn", new Point2D(29, 135));
        souradnice.put("pokoj_rodičů", new Point2D(181, 127));
        souradnice.put("chodba", new Point2D(110, 209));
    }


    public void initialize(IHra hra, Stage primaryStage){
        vytvorSouradnice();
        this.primaryStage = primaryStage;
        hra.getHerniPlan().register(this);
        hra.getHerniPlan().getBatoh().register(this);
        this.hra = hra;
        textShow.setWrapText(true);
        textShow.appendText(hra.vratUvitaniFX());
        terminalText.requestFocus();
        klicInv.setVisible(false);
        chlebaInv.setVisible(false);
        klobasaInv.setVisible(false);
        penizeInv.setVisible(false);
        chlebaKlobasaInv.setVisible(false);





        update();
    }

    /**
     * funkce, ktera aktualizuje seznam vychodu, hrace na mape a inventar
     * take slouzi jako update funkce v navorhovem nazoru Oberver
     */
    @Override
    public void update(){
        //nastaveni pro seznam vedlejsich vychodu
        Prostor aktualniProstor = hra.getHerniPlan().getAktualniProstor();
        seznamVychodu.getItems().clear();
        seznamVychodu.getItems().addAll(aktualniProstor.getVychodyFX());

        //nastaveni pro mapu
        hrac.setX(souradnice.get(aktualniProstor.getNazev()).getX());
        hrac.setY(souradnice.get(aktualniProstor.getNazev()).getY());

        //nasteveni pro inventar
        chlebaInv.setVisible(hra.getHerniPlan().getBatoh().getVeciVBatohu().containsKey("chleba"));
        klicInv.setVisible(hra.getHerniPlan().getBatoh().getVeciVBatohu().containsKey("klič"));
        klobasaInv.setVisible(hra.getHerniPlan().getBatoh().getVeciVBatohu().containsKey("klobasa"));
        chlebaKlobasaInv.setVisible(hra.getHerniPlan().getBatoh().getVeciVBatohu().containsKey("chleba_s_klobasou"));
        penizeInv.setVisible(hra.isPenize());
        terminalText.requestFocus();

    }


    /**
     * zpracuje prikaz pri pouzivani seznamu vychodu
     * @param _prikaz
     */
    public void zpracujPrikaz(String _prikaz){
        textShow.clear();
        String prikaz = _prikaz;
        String odpoved = hra.zpracujPrikaz(prikaz);
        textShow.appendText("Prikaz: "+ prikaz + "\n");;
        textShow.appendText(odpoved + "\n");

        if (hra.konecHry()) {
            textShow.appendText("\n" + hra.vratEpilog());
            terminalText.setDisable(true);
            seznamVychodu.setDisable(true);
            submitButton.setDisable(true);
        }
        terminalText.setText("");
        update();


    }

    /**
     * funkce, ktera spusti tlacitkem "Submit"
     * zpracuje prikaz, ktery byl zadan v pole terminalText
     */
    @FXML
    public void subButton() {
        textShow.clear();
        String odpoved = "";
        String prikaz = terminalText.getText();
        if(prikaz.equals("mluv maminka") && hra.getHerniPlan().getAktualniProstor().getRodic().getKdo().equals("maminka")){

            hraOtaznikFX();



        }else {
            odpoved = hra.zpracujPrikaz(prikaz);

        }
        textShow.appendText(odpoved + "\n");

        if (hra.konecHry()) {
            textShow.appendText("\n" + hra.vratEpilog());
            terminalText.setDisable(true);
            seznamVychodu.setDisable(true);
            submitButton.setDisable(true);
        }
        terminalText.setText("");
        update();
    }


    /**
     * funkce pro zavirani okna, spusti se tlacitken "Ukoncit"
     */
    @FXML
    private void closeButtonAction(){
        Stage stage = (Stage) textShow.getScene().getWindow();
        stage.close();
    }

    /**
     * funkce pro posunu do sousedniho prostoru, vybraneho v seznamu sousednich prostoru
     * @param mouseEvent
     */
    @FXML
    public void vyberProstoru(MouseEvent mouseEvent){

        String prostor = seznamVychodu.getSelectionModel().getSelectedItem().toString();

        String prikaz = PrikazJdi.GetNazev() + " " + prostor;

        zpracujPrikaz(prikaz);


    }

    /**
     * funkce, ktere zobrazuje napovedu
     * @throws IOException
     */
    @FXML public void napovedaMenu() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/napoveda.fxml"));
        VBox parent = loader.load();
        Scene scene = new Scene(parent);
        Stage stage = new Stage();
        stage.setTitle("Nápověda");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();

    }

    /**
     * funkce, ktere slouzi pro spusteni Otaznika
     * cini nove okno
     */
    public void hraOtaznikFX() {

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/otaznik.fxml"));
            VBox root1 =  fxmlLoader.load();
            ControllerOtaznik controllerOtaznik = fxmlLoader.getController();
            controllerOtaznik.initialize(hra,this);

            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(primaryStage);
            stage.setScene(new Scene(root1));
            stage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }


    }

    /**
     * kdyz otanzik se skonci, predava radek textu do hlavneho okna
     * @param str text ktery bude zobrazen v hlavnem okne
     */
    public void otaznikKonec(String str){
        textShow.appendText(str);

    }

    @FXML public void novaHra(){
            textShow.setText("");
            initialize(new Hra(), primaryStage);

    }






}