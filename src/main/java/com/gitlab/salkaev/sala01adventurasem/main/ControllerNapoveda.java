package com.gitlab.salkaev.sala01adventurasem.main;

import javafx.fxml.FXML;
import javafx.scene.web.WebView;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class ControllerNapoveda {



    @FXML
    private WebView webView;

    /**
     * spousti napovedu z HTML documentu
     */
    public void initialize()  {
        webView.getEngine().load(getClass().getResource("/napoveda.html").toExternalForm());
    }

}

