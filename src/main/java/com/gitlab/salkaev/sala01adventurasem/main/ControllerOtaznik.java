package com.gitlab.salkaev.sala01adventurasem.main;

import com.gitlab.salkaev.sala01adventurasem.logika.IHra;
import com.gitlab.salkaev.sala01adventurasem.logika.Otazka;
import com.gitlab.salkaev.sala01adventurasem.logika.Otaznik;
import com.gitlab.salkaev.sala01adventurasem.logika.Rodic;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;

public class ControllerOtaznik extends VBox {


    @FXML public TextArea textShow;
    @FXML public TextField  terminalText;

    IHra hra;
    Rodic maminka;
    ArrayList<Otazka> otaznik;
    Otazka otazka;
    int nahcis;
    int pokus;
    int spravne;
    Controller ctr;

    /**
     * inicializuje Otaznik
     * @param hra to sama hra
     * @param ctr slouzi, aby na konce predat text do hlavneho okna
     */
    public void initialize(IHra hra, Controller ctr){
        textShow.setWrapText(true);
        this.ctr = ctr;
        this.hra = hra;
        pokus = 3;
        spravne = 0;
        maminka = hra.getHerniPlan().getAktualniProstor().getRodic();
        otaznik = hra.getHerniPlan().getOtaznik().getSeznamOtazek();

        textShow.appendText(maminka.getTextKvestNeSplnen());
        textShow.appendText("Maš tři možnosti udělat chybu\n");

        nahcis = Otaznik.GetNahodneCislo(0,otaznik.size() - 1);
        otazka = otaznik.get(nahcis);
        otaznik.remove(nahcis);
        textShow.appendText("Otazka: " + otazka.getOtazka() + "\n");


    }

    /**
     * zpracuje odpoved
     * @param odpoved radek z "terminalText" ktery bude zpracovan
     * @return
     */
    public String zpracujOdpoved(String odpoved){

            String radek = odpoved;
            String vrat;

            if (radek.equals(otazka.getOdpoved())) {
                spravne++;
                vrat = "Spravně!!!Odpoved ještě na " + (3-spravne) + (3-spravne>1?" otazky\n":" otazku\n");
            } else {
                pokus--;
                vrat = "Ne, zkus to zase! Maš " + pokus + (pokus>1?" pokusu\n":" pokus\n");
            }

            return vrat;
    }

    /**
     * kontroluje kolik hrac ma pokusu a kolik spravnych otazek,
     * aby skoncit Otaznik a predat text do hlavneho okna
     */
    public void update(){
        if (pokus == 0) {
            ctr.otaznikKonec("Špatně viš školní ukoly. Mužeš zkusit to zase\n" + maminka.getTextKvestNeSplnen() + "\n");

            Stage stage = (Stage) textShow.getScene().getWindow();
            stage.close();

        }
        if (spravne >= 3) {
            ctr.otaznikKonec("Dobře, odpovědel jsi na 3 otazky\n" + maminka.getTextKvestSplnen() + "\n");
            hra.getHerniPlan().klicViditelny();
            Stage stage = (Stage) textShow.getScene().getWindow();
            stage.close();

        }

        nahcis = Otaznik.GetNahodneCislo(0,otaznik.size() - 1);
        otazka = otaznik.get(nahcis);
        otaznik.remove(nahcis);
        textShow.appendText("Otazka: " + otazka.getOtazka() + "\n");



    }


    /**
     * funkce, ktere se spousti tlacitken "Submit"
     * vezme text z terminalText a preda to do funkce zpracujOdpoved()
     */
    @FXML void subButton(){
        textShow.clear();
        textShow.appendText(zpracujOdpoved(terminalText.getText()));
        terminalText.setText("");
        update();
    }
}
