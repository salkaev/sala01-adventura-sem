/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */

package com.gitlab.salkaev.sala01adventurasem.main;

import com.gitlab.salkaev.sala01adventurasem.logika.Hra;
import com.gitlab.salkaev.sala01adventurasem.logika.IHra;
import com.gitlab.salkaev.sala01adventurasem.uiText.TextoveRozhrani;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.awt.*;


public class Main extends Application
{
    /**
     * main class
     * @param args
     */
    public static void main(String[] args) {
        if(args.length > 0) {
            if(args[0].equals("text")) {
                IHra hra = new Hra();
                TextoveRozhrani ui = new TextoveRozhrani(hra);
                ui.hraj();
            } else {
                System.out.println("Neplatný parametr.\n" +
                        "Použijte parametr 'text' pro spuštění textové veze.");
            }
        } else {
            launch(args);
        }

    }

    /**
     * spousti hlavni okno hry
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/scene.fxml"));
        VBox root = loader.load();
        Controller controller = loader.getController();
        controller.initialize(new Hra(),primaryStage);
        primaryStage.setScene(new Scene(root));
        primaryStage.setTitle("Adventura");
        primaryStage.setResizable(false);
        primaryStage.show();
    }



}
