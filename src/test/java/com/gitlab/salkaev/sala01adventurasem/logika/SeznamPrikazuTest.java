package com.gitlab.salkaev.sala01adventurasem.logika;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


/*******************************************************************************
 * Testovací třída SeznamPrikazuTest slouží ke komplexnímu otestování třídy  
 * SeznamPrikazu
 * 
 * @author    Luboš Pavlíček
 * @version   pro školní rok 2016/2017
 */
public class SeznamPrikazuTest
{
    private Hra hra;
    private PrikazKonec prKonec;
    private PrikazJdi prJdi;
    private PrikazSeber prSeber;
    private PrikazObsahBatohu prObsBat;
    private PrikazMluvit prMluvit;
    private PrikazKombinovat prKombin;
    
    @Before
    public void setUp() {
        hra = new Hra();
        prKonec = new PrikazKonec(hra);
        prJdi = new PrikazJdi(hra.getHerniPlan(),hra);
        prSeber = new PrikazSeber(hra.getHerniPlan());
        prObsBat = new PrikazObsahBatohu(hra.getHerniPlan());
        prMluvit = new PrikazMluvit(hra.getHerniPlan(),hra);
        prKombin = new PrikazKombinovat(hra.getHerniPlan());
    }

    @Test
    public void testVlozeniVybrani() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prJdi);
        seznPrikazu.vlozPrikaz(prSeber);
        seznPrikazu.vlozPrikaz(prObsBat);
        seznPrikazu.vlozPrikaz(prMluvit);
        seznPrikazu.vlozPrikaz(prKombin);
        Assert.assertEquals(prKonec, seznPrikazu.vratPrikaz("konec"));
        Assert.assertEquals(prJdi, seznPrikazu.vratPrikaz("jdi"));
        Assert.assertEquals(null, seznPrikazu.vratPrikaz("nápověda"));
        Assert.assertEquals(prSeber, seznPrikazu.vratPrikaz("seber"));
        Assert.assertEquals(prObsBat, seznPrikazu.vratPrikaz("obsah_batohu"));
        Assert.assertEquals(prMluvit, seznPrikazu.vratPrikaz("mluv"));
        Assert.assertEquals(prKombin, seznPrikazu.vratPrikaz("kombinuj"));
    }
    @Test
    public void testJePlatnyPrikaz() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prJdi);
        seznPrikazu.vlozPrikaz(prSeber);
        seznPrikazu.vlozPrikaz(prObsBat);
        seznPrikazu.vlozPrikaz(prMluvit);
        seznPrikazu.vlozPrikaz(prKombin);
        Assert.assertEquals(true, seznPrikazu.jePlatnyPrikaz("konec"));
        Assert.assertEquals(true, seznPrikazu.jePlatnyPrikaz("jdi"));
        Assert.assertEquals(false, seznPrikazu.jePlatnyPrikaz("nápověda"));
        Assert.assertEquals(false, seznPrikazu.jePlatnyPrikaz("Konec"));
        Assert.assertEquals(true, seznPrikazu.jePlatnyPrikaz("seber"));
        Assert.assertEquals(true, seznPrikazu.jePlatnyPrikaz("obsah_batohu"));
        Assert.assertEquals(true, seznPrikazu.jePlatnyPrikaz("mluv"));
        Assert.assertEquals(true, seznPrikazu.jePlatnyPrikaz("kombinuj"));
    }
    
    @Test
    public void testNazvyPrikazu() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prJdi);
        seznPrikazu.vlozPrikaz(prSeber);
        seznPrikazu.vlozPrikaz(prObsBat);
        seznPrikazu.vlozPrikaz(prMluvit);
        seznPrikazu.vlozPrikaz(prKombin);
        String nazvy = seznPrikazu.vratNazvyPrikazu();
        Assert.assertEquals(true, nazvy.contains("konec"));
        Assert.assertEquals(true, nazvy.contains("jdi"));
        Assert.assertEquals(false, nazvy.contains("nápověda"));
        Assert.assertEquals(true, nazvy.contains("seber"));
        Assert.assertEquals(true, nazvy.contains("obsah_batohu"));
        Assert.assertEquals(true, nazvy.contains("mluv"));
        Assert.assertEquals(true, nazvy.contains("kombinuj"));
    }
    
}
